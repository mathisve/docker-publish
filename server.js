const express = require('express');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
app.get('/', (req, res) => {
    console.log("hit /")
    res.send('Hello World!');
});

app.get('/hello', (req, res) => {
    console.log("hit /hello")
    res.send("Hi there!");
})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);